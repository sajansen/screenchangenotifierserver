package nl.sajansen.screenchangenotifierserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.scheduling.annotation.EnableScheduling


@SpringBootApplication
@EnableScheduling
class ScreenChangeNotifierServerApplication

fun main(args: Array<String>) {
    val builder = SpringApplicationBuilder(ScreenChangeNotifierServerApplication::class.java)
    builder.headless(false).run(*args)
//    runApplication<ScreenChangeNotifierServerApplication>(*args)
}
