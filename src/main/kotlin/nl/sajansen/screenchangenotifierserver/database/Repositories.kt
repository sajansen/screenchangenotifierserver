package nl.sajansen.screenchangenotifierserver.database

import org.springframework.data.repository.CrudRepository

interface ScreenshotRepository : CrudRepository<Screenshot, Long> {
    fun findFirstByOrderByLastModifiedDesc(): Screenshot?
}