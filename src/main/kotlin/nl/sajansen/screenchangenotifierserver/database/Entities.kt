package nl.sajansen.screenchangenotifierserver.database

import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id


enum class ScreenshotType {
    UNKNOWN,
    BLACK,
    SOLID_COLOR,
    INFORMATION,
    SONG,
    SCRIPTURE,
    PRESENTATION,
    VIDEO,
    DESKTOP
}

@Entity
data class Screenshot(
    var filePath: String = "",
    var fileName: String = "",
    var fileSize: Long? = null,
    var type: ScreenshotType = ScreenshotType.UNKNOWN,
    @UpdateTimestamp var lastModified: LocalDateTime = LocalDateTime.now(),
    var changeNumber: Long? = null,
    @Id @GeneratedValue var id: Long? = null
)