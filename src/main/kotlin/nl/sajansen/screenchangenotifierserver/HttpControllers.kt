package nl.sajansen.screenchangenotifierserver

import nl.sajansen.screenchangenotifierserver.database.Screenshot
import nl.sajansen.screenchangenotifierserver.database.ScreenshotRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.FileSystemResource
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.io.File
import javax.servlet.http.HttpServletResponse

@RestController
class PlainController {
    @GetMapping("/status")
    fun getStatus(): String {
        return "running"
    }
}

@RestController
@RequestMapping("/api/screenshots")
class ScreenshotController(private val screenshotRepository: ScreenshotRepository) {
    @GetMapping("/")
    fun getAll() = screenshotRepository.findAll()

    @GetMapping("/{id}")
    fun getByID(@PathVariable id: Long) =
        screenshotRepository.findByIdOrNull(id) ?: throw IllegalArgumentException("ID does not exists")

    @GetMapping("/latest")
    fun getLatest() = screenshotRepository.findFirstByOrderByLastModifiedDesc()
        ?: throw IllegalArgumentException("No latest screenshot entity found")
}

@Controller
@RequestMapping("/media")
class MediaController(private val screenshotRepository: ScreenshotRepository) {

    @Value("\${app.httpcontroller.media.forcedownload:false}")
    var forceDownload: Boolean = false

    @GetMapping("/screenshots/{id}", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    @ResponseBody
    fun getByID(@PathVariable id: Long, httpServletResponse: HttpServletResponse): FileSystemResource {
        println("Media request for id: $id")
        val screenshotEntity =
            screenshotRepository.findByIdOrNull(id) ?: throw IllegalArgumentException("ID does not exists")

        return serveScreenshotImage(httpServletResponse, screenshotEntity)
    }

    @GetMapping("/screenshots/latest", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    @ResponseBody
    fun getLatest(httpServletResponse: HttpServletResponse): FileSystemResource {
        println("Media request for latest screenshot")
        val screenshotEntity =
            screenshotRepository.findFirstByOrderByLastModifiedDesc()
                ?: throw IllegalArgumentException("No latest screenshot entity found")

        return serveScreenshotImage(httpServletResponse, screenshotEntity)
    }

    private fun serveScreenshotImage(
        httpServletResponse: HttpServletResponse,
        screenshotEntity: Screenshot
    ): FileSystemResource {
        if (forceDownload)
            httpServletResponse.setHeader(
                "Content-Disposition",
                "attachment; filename=${screenshotEntity.fileName}"
            )

        val screenshotFile = File(screenshotEntity.filePath)

        if (screenshotFile.length() != screenshotEntity.fileSize) {
            println("Real file size: ${screenshotFile.length()}, expected file size: ${screenshotEntity.fileSize}")
            println("Wait a moment before we can read")
            Thread.sleep(800)
        }

        println("Serving the file with size: ${screenshotFile.length()}.")
        return FileSystemResource(screenshotFile)
    }
}