//package nl.sajansen.screenchangenotifierserver
//
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.context.annotation.Configuration
//import org.springframework.messaging.handler.annotation.MessageMapping
//import org.springframework.messaging.simp.SimpMessagingTemplate
//import org.springframework.messaging.simp.config.MessageBrokerRegistry
//import org.springframework.scheduling.annotation.Scheduled
//import org.springframework.stereotype.Controller
//import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
//import org.springframework.web.socket.config.annotation.StompEndpointRegistry
//import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
//import java.time.Instant
//import java.time.format.DateTimeFormatter
//
//
//@Configuration
//@EnableWebSocketMessageBroker
//class WebSocketConfig : WebSocketMessageBrokerConfigurer {
//    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
//        registry.addEndpoint("/ws").setAllowedOrigins("*")
//    }
//
//    override fun configureMessageBroker(config: MessageBrokerRegistry) {
//        config.enableSimpleBroker("/topic", "/queue")
//        config.setApplicationDestinationPrefixes("/app")
//    }
//}
//
//@Controller
//class WebsocketController @Autowired constructor(val template: SimpMessagingTemplate) {
////    @Scheduled(fixedDelayString = "1000")
//    fun blastToClientsHostReport() {
//        println("Sending something on the websocket")
//        template.convertAndSend("/topic/greeting", "Hello World");
//    }
//
//    @MessageMapping("/greeting")
//    fun handle(message: String): String {
//        println("Received message: $message")
//        template.convertAndSend("/topic/greeting", message)
//        return "[" + getTimestamp() + ": " + message
//    }
//}
//
//fun getTimestamp(): String = DateTimeFormatter.ISO_INSTANT.format(Instant.now())
