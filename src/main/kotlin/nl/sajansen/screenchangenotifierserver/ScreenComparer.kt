package nl.sajansen.screenchangenotifierserver

import nl.sajansen.screenchangenotifierserver.database.Screenshot
import nl.sajansen.screenchangenotifierserver.database.ScreenshotRepository
import nl.sajansen.screenchangenotifierserver.database.ScreenshotType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.awt.Color
import java.awt.GraphicsEnvironment
import java.awt.Rectangle
import java.awt.Robot
import java.awt.image.BufferedImage
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicLong
import javax.annotation.PreDestroy
import javax.imageio.ImageIO

@Component
class ScreenComparer(@Autowired private var screenshotRepository: ScreenshotRepository) {
    private var timer: Timer? = null
    private val changeNumber = AtomicLong()
    val outputImageDir: File = createTempDir(suffix = "screenchangenotifierserver").apply {
        deleteOnExit() // Doesn't work with full folder, but we have a @Destroy hook for that
    }
//    val outputImageDir: File = File("media/screenshots")

    @Value("\${app.comparer.output.image.prefix:screenshot_}")
    var outputImagePrefix: String? = "screenshot_"

    @Value("\${app.comparer.output.image.suffix:}")
    var outputImageSuffix: String? = ""

    @Value("\${app.comparer.output.image.fileformat:png}")
    var outputImageFileFormat: String? = "png"

    @Value("\${app.comparer.delete.oldfiles:false}")
    var deleteOldImages: Boolean? = false

    @Value("\${app.comparer.delete.files.onexit:true}")
    var deleteImageDirOnExit: Boolean? = true

    @Value("\${app.comparer.interval:1000}")
    var timerIntervalMs: Long? = 1000L

    @Value("\${app.comparer.screen.number:0}")
    var screenNumber: Int? = 0

    @Value("\${app.comparer.screen.offset.x:}")
    var screenOffsetX: Int? = 0
    @Value("\${app.comparer.screen.offset.y:}")
    var screenOffsetY: Int? = 0
    @Value("\${app.comparer.screen.offset.width:}")
    var screenOffsetWidth: Int? = 0
    @Value("\${app.comparer.screen.offset.height:}")
    var screenOffsetHeight: Int? = 0

    fun startComparingScreenshots() {
        timer = Timer()
        timer?.schedule(object : TimerTask() {
            override fun run() {
                compareScreenshots()
            }
        }, 0, timerIntervalMs ?: 0)
    }

    fun generateOutputFilePath(id: Long): String =
        "${outputImageDir.absolutePath}${File.separator}$outputImagePrefix$id$outputImageSuffix.$outputImageFileFormat"

    @Scheduled(fixedDelayString = "\${app.comparer.interval:1000}")
    fun compareScreenshots() {
        println("Comparing screenshots")
        val newBufferedImage = takeScreenshot(screen = screenNumber ?: 0)

        val lastImageFile = File(generateOutputFilePath(changeNumber.get()))
        if (!lastImageFile.exists()) {
            saveNewImage(newBufferedImage)
            return
        }

        val lastBufferedImage: BufferedImage
        try {
            lastBufferedImage = ImageIO.read(lastImageFile)
        } catch (e: IndexOutOfBoundsException) {
            println(e.stackTrace)
            lastImageFile.delete()
            return
        }

        if (compareBufferedImages(lastBufferedImage, newBufferedImage))
            return

        saveNewImage(newBufferedImage)
        if (deleteOldImages == true)
            lastImageFile.delete()
    }

    private fun saveNewImage(newBufferedImage: BufferedImage) {
        val outputFile = File(generateOutputFilePath(changeNumber.incrementAndGet()))
        println("Saving new image nr. $changeNumber to $outputFile")

        ImageIO.write(newBufferedImage, outputImageFileFormat, outputFile)

        val screenShotEntity = Screenshot(
            filePath = outputFile.path,
            fileName = outputFile.name,
            fileSize = outputFile.length(),
            type = determineImageType(newBufferedImage),
            changeNumber = changeNumber.get()
        )
        screenshotRepository.save(screenShotEntity)
    }

    fun compareBufferedImages(bufferedImageA: BufferedImage, bufferedImageB: BufferedImage): Boolean {
        if (bufferedImageA.width != bufferedImageB.width || bufferedImageA.height != bufferedImageB.height)
            return false

        for (y in 0 until bufferedImageA.height) {
            for (x in 0 until bufferedImageA.width) {
                if (bufferedImageA.getRGB(x, y) != bufferedImageB.getRGB(x, y))
                    return false
            }
        }

        return true
    }

    fun determineImageType(bufferedImage: BufferedImage): ScreenshotType {
        val maxBlackColorTint = Color(30, 30, 30)
        var isSolidColor = true
        val solidColorRGB = bufferedImage.getRGB(0, 0)

        for (y in 0 until bufferedImage.height) {
            for (x in 0 until bufferedImage.width) {
                if (bufferedImage.getRGB(x, y) != solidColorRGB)
                    isSolidColor = false
            }
        }

        return when (true) {
            isSolidColor -> {
                if (getLuminanceOfRGB(solidColorRGB) < getLuminanceOfRGB(maxBlackColorTint.rgb))
                    return ScreenshotType.BLACK
                return ScreenshotType.SOLID_COLOR
            }
            else -> ScreenshotType.UNKNOWN
        }
    }

    fun getLuminanceOfRGB(rgb: Int): Float {
        val red = (rgb and 0x00ff0000) ushr 16
        val green = (rgb and 0x0000ff00) ushr 8
        val blue = rgb and 0x000000ff
        return ((red * 0.21 + green * 0.72 + blue * 0.72) / 420.75).toFloat()
    }

    private fun takeScreenshot(screen: Int = 0): BufferedImage {
        val screenDevices = GraphicsEnvironment.getLocalGraphicsEnvironment().screenDevices
        val screenBounds = screenDevices[screen].defaultConfiguration.bounds
        val screenRectangle = Rectangle(screenBounds)

        screenRectangle.x += screenOffsetX ?: 0
        screenRectangle.y += screenOffsetY ?: 0
        if (screenOffsetWidth?:0 > 0) {
            screenRectangle.width = screenOffsetWidth ?: 0
        }
        if (screenOffsetHeight?:0 > 0) {
            screenRectangle.height = screenOffsetHeight ?: 0
        }

        return Robot().createScreenCapture(screenRectangle)
    }

    fun clearDirectory(directory: File?) {
        if (directory == null || !directory.isDirectory) {
            return
        }

        println("Clearing directory")
        for (subFile in directory.listFiles()!!) {
            println("Deleting file: " + subFile.absolutePath)
            if (!subFile.delete()) {
                println("Failed to delete file: " + subFile.absolutePath)
            }
        }

        directory.delete()
    }

    @PreDestroy
    fun destroy() {
        if (deleteImageDirOnExit != false)
            clearDirectory(outputImageDir)
    }
}