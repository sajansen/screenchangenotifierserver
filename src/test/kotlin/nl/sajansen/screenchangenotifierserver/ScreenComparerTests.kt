package nl.sajansen.screenchangenotifierserver

import com.ninjasquad.springmockk.MockkBean
import nl.sajansen.screenchangenotifierserver.database.ScreenshotRepository
import nl.sajansen.screenchangenotifierserver.database.ScreenshotType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import java.awt.Color
import java.io.File
import javax.imageio.ImageIO

@WebMvcTest
class ScreenComparerTests {

    @MockkBean
    private lateinit var repository: ScreenshotRepository

    @Test
    fun `generated name gives correct value`() {
        val screenComparer = ScreenComparer(repository)
        assertThat(screenComparer.generateOutputFilePath(7)).isEqualTo("${screenComparer.outputImageDir.absolutePath}/screenshot_7.png")

    }

    @Test
    fun `compare images vergelijkt twee images die niet dezelfde zijn`() {
        val imageFileA = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/00_Item_1.jpg"))
        val imageFileB = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/00_Item_2.jpg"))
        val result = ScreenComparer(repository).compareBufferedImages(imageFileA, imageFileB)

        assertThat(result).isFalse()
    }

    @Test
    fun `compare images vergelijkt twee images die wel dezelfde zijn`() {
        val imageFileA = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/00_Item_1.jpg"))
        val imageFileB = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/01_Item_1.jpg"))
        val result = ScreenComparer(repository).compareBufferedImages(imageFileA, imageFileB)

        assertThat(result).isTrue()
    }

    @Test
    fun `get luminance of black returns zero`() {
        val result = ScreenComparer(repository).getLuminanceOfRGB(Color.BLACK.rgb)

        assertThat(result).isEqualTo(0.0f)
    }

    @Test
    fun `get luminance of white returns one`() {
        val result = ScreenComparer(repository).getLuminanceOfRGB(Color.WHITE.rgb)

        assertThat(result).isEqualTo(1.0f)
    }

    @Test
    fun `get screenshot with black returns black type`() {
        val imageFile = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/type_black.jpg"))
        val result = ScreenComparer(repository).determineImageType(imageFile)

        assertThat(result).isEqualTo(ScreenshotType.BLACK)
    }

    @Test
    fun `get screenshot with blue color returns blue type`() {
        val imageFile = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/type_solid_color.jpg"))
        val result = ScreenComparer(repository).determineImageType(imageFile)

        assertThat(result).isEqualTo(ScreenshotType.SOLID_COLOR)
    }

    @Test
    fun `get screenshot of an unknown image returns unknown type`() {
        val imageFile = ImageIO.read(File("/home/prive/IdeaProjects/ScreenChangeNotifierServer/src/test/resources/type_unknown.jpg"))
        val result = ScreenComparer(repository).determineImageType(imageFile)

        assertThat(result).isEqualTo(ScreenshotType.UNKNOWN)
    }
}