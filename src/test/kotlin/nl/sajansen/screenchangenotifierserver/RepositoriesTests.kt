package nl.sajansen.screenchangenotifierserver

import nl.sajansen.screenchangenotifierserver.database.Screenshot
import nl.sajansen.screenchangenotifierserver.database.ScreenshotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull
import java.time.LocalDateTime

@DataJpaTest
class RepositoriesTests @Autowired constructor(
    val entityManager: TestEntityManager,
    val screenshotRepository: ScreenshotRepository
) {

    @BeforeEach
    fun setup() {
        entityManager.clear()
    }

    @Test
    fun `When findByIdOrNull then return Screenshot`() {
        val screenshot1 = Screenshot()
        entityManager.persist(screenshot1)
        entityManager.flush()

        val result = screenshotRepository.findByIdOrNull(screenshot1.id!!)
        assertThat(result).isEqualTo(screenshot1)
    }

    @Test
    fun `When findFirstByOrderByLastModifiedDesc then return latest Screenshot`() {
        val screenshot1 = Screenshot(lastModified = LocalDateTime.of(2019, 10, 16, 9, 30, 10, 0))
        val screenshot2 = Screenshot(lastModified = LocalDateTime.of(2019, 10, 16, 10, 30, 20, 0))
        val screenshot3 = Screenshot(lastModified = LocalDateTime.of(2019, 10, 16, 9, 30, 21, 0))
        entityManager.persist(screenshot1)
        entityManager.flush()
        Thread.sleep(1)
        entityManager.persist(screenshot3)
        entityManager.flush()
        Thread.sleep(1)
        entityManager.persist(screenshot2)
        entityManager.flush()

        val result = screenshotRepository.findFirstByOrderByLastModifiedDesc()
        print(result)
        assertThat(result).isEqualTo(screenshot2)
    }
}