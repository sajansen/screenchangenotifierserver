package nl.sajansen.screenchangenotifierserver

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import nl.sajansen.screenchangenotifierserver.database.Screenshot
import nl.sajansen.screenchangenotifierserver.database.ScreenshotRepository
import nl.sajansen.screenchangenotifierserver.database.ScreenshotType
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers


@WebMvcTest
class HttpControllersTests(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    private lateinit var screenshotRepository: ScreenshotRepository

    @Test
    fun `List screenshots`() {
        val screenshot1 = Screenshot(fileName = "screenshot1.png")
        val screenshot2 = Screenshot(fileName = "screenshot2.png")
        val screenshot3 = Screenshot(fileName = "screenshot3.png")
        every { screenshotRepository.findAll() } returns listOf(screenshot1, screenshot2, screenshot3)

        mockMvc.perform(MockMvcRequestBuilders.get("/api/screenshots/").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("\$.[0].fileName").value(screenshot1.fileName))
            .andExpect(MockMvcResultMatchers.jsonPath("\$.[0].type").value(ScreenshotType.UNKNOWN.toString()))
            .andExpect(MockMvcResultMatchers.jsonPath("\$.[1].fileName").value(screenshot2.fileName))
    }

    @Test
    fun `Latest screenshot`() {
        val screenshot1 = Screenshot(fileName = "screenshot1.png")
        every { screenshotRepository.findFirstByOrderByLastModifiedDesc() } returns screenshot1

        mockMvc.perform(MockMvcRequestBuilders.get("/api/screenshots/latest").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(MockMvcResultMatchers.jsonPath("\$.fileName").value(screenshot1.fileName))
    }
}