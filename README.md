# ScreenChangeNotifierServer

_Captures the content of the screen and serves this on a HTTP webserver with an API._

Each `app.comparer.interval` milliseconds, a screenshot is made of the configured screen `app.comparer.screen.number`. When this screenshot differs from the latest screenshot, the new screenshot will be served as a new file. Use [/api/screenshots/latest](http://localhost:8080/api/screenshots/latest) to get the latest screenshot information. Use [/media/screenshots/\<id>](http://localhost:8080/media/screenshots/\<id>) to download the screenshot image or just use [/media/screenshots/latest](http://localhost:8080/media/screenshots/latest) to download the latest screenshot. 